import React, { Suspense } from "react";
import { Router, Switch, Redirect, Route } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";

import { customHistory } from "../config";

import Products from "../../pages/products/products";
import Orders from "../../pages/orders/orders";

const Routes = () => {
	return (
		<Suspense fallback={<div>Loading...</div>}>
			<ConnectedRouter history={customHistory}>
				<Switch>
					<Route path="/products" component={Products} />
					<Route path="/orders" component={Orders} />
					<Redirect to="/products" />
				</Switch>
			</ConnectedRouter>
		</Suspense>
	);
};

export default Routes;
