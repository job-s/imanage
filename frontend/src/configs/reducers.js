import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import { productsReducer } from "../pages/products/ducks";
import { ordersReducer } from "../pages/orders/ducks";

const createRootReducer = (customHistory) =>
	combineReducers({
		router: connectRouter(customHistory),
		productsReducer,
		ordersReducer,
	});

export default createRootReducer;
