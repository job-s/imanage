import { all } from "redux-saga/effects";

import { productsSaga } from "../pages/products/ducks";
import { ordersSaga } from "../pages/orders/ducks";

export default function* () {
	yield all([productsSaga(), ordersSaga()]);
}
