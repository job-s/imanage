export const productsSchema = {
	title: "Products",
	type: "object",
	properties: {
		Food: {
			type: "string",
			enum: ["Lentil", "Rice", "Milk", "Vegetables"],
			default: "Lentil",
		},
		Utensils: {
			type: "string",
			enum: ["Tea sets", "Spoons", "Plates"],
		},
		Toys: {
			type: "string",
			enum: ["Electric toys", "Board games", "Card games"],
		},
	},
	dependencies: {
		Food: {
			oneOf: [
				{
					properties: {
						Food: {
							enum: ["Lentil"],
						},
						Lentil: {
							type: "string",
							enum: ["Masoor Dal", "Toor/Arhar Dal", "Lobia", "Chickpea"],
						},
					},
				},
				{
					properties: {
						Food: {
							enum: ["Rice"],
						},
						Rice: {
							type: "string",
							enum: ["Basmati Rice", "Brown Rice", "Jasmine Rice"],
						},
					},
				},
				{
					properties: {
						Food: {
							enum: ["Milk"],
						},
						Milk: {
							type: "string",
							enum: ["Silm Milk", "Cow Milk"],
						},
					},
				},
				{
					properties: {
						Food: {
							enum: ["Vegetables"],
						},
						Vegetables: {
							type: "string",
							enum: ["Tomato", "Potato", "Peas"],
						},
					},
				},
			],
		},
		Lentil: {
			oneOf: [
				{
					properties: {
						Lentil: {
							enum: ["Masoor Dal"],
						},
						"Masoor Dal": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Masoor Dal"],
				},
				{
					properties: {
						Lentil: {
							enum: ["Toor/Arhar Dal"],
						},
						"Toor/Arhar Dal": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Toor/Arhar Dal"],
				},
				{
					properties: {
						Lentil: {
							enum: ["Lobia"],
						},
						Lobia: {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Lobia"],
				},
				{
					properties: {
						Lentil: {
							enum: ["Chickpea"],
						},
						Chickpea: {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Chickpeas"],
				},
			],
		},
		Rice: {
			oneOf: [
				{
					properties: {
						Rice: {
							enum: ["Basmati Rice"],
						},
						"Basmati Rice": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Basmati Rice"],
				},
				{
					properties: {
						Rice: {
							enum: ["Brown Rice"],
						},
						"Brown Rice": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Brown Rice"],
				},
				{
					properties: {
						Rice: {
							enum: ["Jasmine Rice"],
						},
						"Jasmine Rice": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Jasmine Rice"],
				},
			],
		},
		Milk: {
			oneOf: [
				{
					properties: {
						Milk: {
							enum: ["Silm Milk"],
						},
						"Silm Milk": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Silm Milk"],
				},
				{
					properties: {
						Milk: {
							enum: ["Cow Milk"],
						},
						"Cow Milk": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Cow Milk"],
				},
			],
		},
		Vegetables: {
			oneOf: [
				{
					properties: {
						Vegetables: {
							enum: ["Tomato"],
						},
						Tomato: {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Tomato"],
				},
				{
					properties: {
						Vegetables: {
							enum: ["Potato"],
						},
						Potato: {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Potato"],
				},
				{
					properties: {
						Vegetables: {
							enum: ["Peas"],
						},
						Peas: {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Peas"],
				},
			],
		},
		Utensils: {
			oneOf: [
				{
					properties: {
						Utensils: {
							enum: ["Tea sets"],
						},
						"Tea sets": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Tea sets"],
				},
				{
					properties: {
						Utensils: {
							enum: ["Spoons"],
						},
						Spoons: {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Spoons"],
				},
				{
					properties: {
						Utensils: {
							enum: ["Plates"],
						},
						Plates: {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Plates"],
				},
			],
		},
		Toys: {
			oneOf: [
				{
					properties: {
						Toys: {
							enum: ["Electric toys"],
						},
						"Electric toys": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Electric toys"],
				},
				{
					properties: {
						Toys: {
							enum: ["Board games"],
						},
						"Board games": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Board games"],
				},
				{
					properties: {
						Toys: {
							enum: ["Card games"],
						},
						"Card games": {
							type: "integer",
							minimum: 1,
						},
					},
					required: ["Card games"],
				},
			],
		},
	},
};

export const productsUISchema = {
	"ui:order": [
		"Food",
		"Lentil",
		"Masoor Dal",
		"Toor/Arhar Dal",
		"Lobia",
		"Chickpea",
		"Rice",
		"Basmati Rice",
		"Brown Rice",
		"Jasmine Rice",
		"Milk",
		"Silm Milk",
		"Cow Milk",
		"Vegetables",
		"Tomato",
		"Potato",
		"Peas",
		"Utensils",
		"Tea sets",
		"Spoons",
		"Plates",
		"Toys",
		"Electric toys",
		"Board games",
		"Card games",
	],
	Food: {
		isClearable: true,
	},
	Lentil: {
		isClearable: true,
	},
	Rice: {
		isClearable: true,
	},
	Milk: {
		isClearable: true,
	},
	Vegetables: {
		isClearable: true,
	},
	Utensils: {
		isClearable: true,
	},
	Toys: {
		isClearable: true,
	},
	"Masoor Dal": {
		unit: "kg",
	},
	"Toor/Arhar Dal": {
		unit: "kg",
	},
	Lobia: {
		unit: "kg",
	},
	Chickpea: {
		unit: "kg",
	},
	"Basmati Rice": {
		unit: "kg",
	},
	"Brown Rice": {
		unit: "kg",
	},
	"Jasmine Rice": {
		unit: "kg",
	},
	"Silm Milk": {
		unit: "kg",
	},
	"Cow Milk": {
		unit: "kg",
	},
	Tomato: {
		unit: "kg",
	},
	Potato: {
		unit: "kg",
	},
	Peas: {
		unit: "kg",
	},
	"Tea sets": {
		unit: "pc",
	},
	Spoons: {
		unit: "pc",
	},
	Plates: {
		unit: "pc",
	},
	"Electric toys": {
		unit: "pc",
	},
	"Board games": {
		unit: "pc",
	},
	"Card games": {
		unit: "pc",
	},
};
