import React from "react";
import Select from "react-select";

export const DropDown = (props) => {
	const {
		onChange,
		options: { enumOptions = [] },
		value,
		uiSchema: { isClearable = false },
	} = props;
	const _onChange = (item) => {
		onChange(item?.value || undefined);
	};

	return (
		<Select
			options={enumOptions}
			onChange={_onChange}
			defaultValue={enumOptions.find((o) => o.value === value)}
			isClearable={isClearable}
		/>
	);
};
