import React, { useEffect } from "react";
import { connect } from "react-redux";
import Select from "react-select";
import Form from "@rjsf/core";

import { productsSchema, productsUISchema } from "../../schemas/productsSchema";

const FormWrapper = ({
	formData,
	disabled = false,
	updateRecord,
	onCreate,
	fields,
	widgets,
}) => {
	const onSubmit = (data) => {
		if (formData?._id) {
			fetch(process.env.SERVER + "api/orders/update/" + formData._id, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(data.formData),
			})
				.then((res) => res.json())
				.then((result) => {
					if (result?.nModified === 1) {
						updateRecord(data.formData);
					}
				});
		} else {
			fetch(process.env.SERVER + "api/orders/placeorder", {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(data.formData),
			})
				.then((res) => res.json())
				.then((result) => {
					onCreate();
				});
		}
	};

	const onError = (error) => {
		console.error(error);
	};

	return (
		<Form
			schema={productsSchema}
			uiSchema={productsUISchema}
			onSubmit={onSubmit}
			formData={formData}
			disabled={disabled}
			onError={onError}
			fields={fields}
			widgets={widgets}
		/>
	);
};

export { FormWrapper };
