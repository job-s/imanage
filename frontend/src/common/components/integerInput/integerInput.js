import React from "react";

import "./integerInput.scss";

export const IntegerInput = (props) => {
	const {
		onChange,
		uiSchema: { unit = "kg" },
		formData,
	} = props;
	const _onChange = (event) => {
		onChange(parseInt(event.currentTarget.value) || undefined);
	};

	return (
		<div className="integer-input">
			<input
				type="number"
				onChange={_onChange}
				defaultValue={formData || undefined}
			/>
			<span>{unit}</span>
		</div>
	);
};
