import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { push } from "connected-react-router";

import "./header.scss";

const Header = ({ push }) => {
	return (
		<div className="header">
			<Link to="/products">Products</Link>
			<Link to="/orders">Orders</Link>
		</div>
	);
};

const enhance = connect((state) => ({}), { push });

export default enhance(Header);
