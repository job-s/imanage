import React, { useEffect } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import { FormWrapper } from "../../common/components/formWrapper/formWrapper";
import Header from "../../common/components/header/header";
import { DropDown } from "../../common/components/dropDown/dropDown";
import { IntegerInput } from "../../common/components/integerInput/integerInput";

import { getOrderSelector, updateOrderAction } from "../orders/ducks";

import "./products.scss";

const fields = {
	NumberField: IntegerInput,
};

const widgets = {
	SelectWidget: DropDown,
};

const Products = ({ order, updateOrder, location, push }) => {
	const onCreate = () => {
		push("/orders");
	};

	return (
		<div className="products-page">
			<Header />
			<FormWrapper
				formData={order}
				updateRecord={updateOrder}
				onCreate={onCreate}
				fields={fields}
				widgets={widgets}
			/>
		</div>
	);
};

const enhance = connect(
	(state, ownProps) => ({
		order: getOrderSelector(state, ownProps?.location?.state?._id),
	}),
	{
		updateOrder: updateOrderAction,
		push,
	}
);

export default enhance(Products);
