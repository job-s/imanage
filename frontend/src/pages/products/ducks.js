import { createAction, createReducer, createSelector } from "@reduxjs/toolkit";
import { put, all, takeLatest } from "redux-saga/effects";
import { push } from "connected-react-router";

const DO_SOMETHING = "[products] do something";
const SOMETHING_DONE = "[products] something done";

const initialState = {
	loading: true,
};

export const stateSelector = (state) => state.productsReducer;

export const getLoading = createSelector(stateSelector, (state) => {
	console.log(state);
	return state.loading;
});

export const productsReducer = createReducer(initialState, {
	[SOMETHING_DONE]: (state, { payload }) => {
		state.loading = false;
	},
});

export const doSomething = createAction(DO_SOMETHING);

function* initProducts({ payload }) {
	yield put({ type: SOMETHING_DONE, payload: { loading: false } });
}

export function* productsSaga() {
	yield all([yield takeLatest(DO_SOMETHING, initProducts)]);
}
