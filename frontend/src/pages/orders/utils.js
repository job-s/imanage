export const fetchOrders = () => {
	return fetch(process.env.SERVER + "api/orders/")
		.then((res) => res.json())
		.then((result) => result);
};
