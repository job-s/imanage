import { createAction, createReducer, createSelector } from "@reduxjs/toolkit";
import { put, all, takeLatest, call } from "redux-saga/effects";
import { push } from "connected-react-router";
import { fetchOrders } from "./utils";

const GET_ORDERS = "[orders] get orders";
const ORDERS_RECEIVED = "[orders] orders received";
const UPDATE_ORDER = "[orders] update order";
const DELETE_ORDER = "[order] delete order";

const initialState = {
	loading: true,
	orders: [],
};

export const stateSelector = (state) => state.ordersReducer;
export const getOrdersSelector = (state) => stateSelector(state).orders;
export const getOrderSelector = (state, _id) =>
	stateSelector(state).orders.find((o) => o._id === _id);

export const ordersReducer = createReducer(initialState, {
	[ORDERS_RECEIVED]: (state, { payload }) => {
		state.orders = payload;
		state.loading = false;
	},
	[UPDATE_ORDER]: (state, { payload }) => {
		console.log("payload", payload);
		for (const o of state.orders) {
			if (o._id === payload._id) {
				Object.assign(o, payload);
				break;
			}
		}
	},
	[DELETE_ORDER]: (state, { payload }) => {
		const odrs = state.orders.filter((o) => o._id !== payload);
		state.orders = odrs;
	},
});

export const getOrdersAction = createAction(GET_ORDERS);
export const updateOrderAction = createAction(UPDATE_ORDER);
export const deleteOrderAction = createAction(DELETE_ORDER);

function* getOrdersGen({ payload }) {
	const orders = yield call(fetchOrders);
	yield put({ type: ORDERS_RECEIVED, payload: orders });
}

export function* ordersSaga() {
	yield all([yield takeLatest(GET_ORDERS, getOrdersGen)]);
}
