import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { FormWrapper } from "../../common/components/formWrapper/formWrapper";
import Header from "../../common/components/header/header";

import { getOrdersSelector, getOrdersAction, deleteOrderAction } from "./ducks";

import "./orders.scss";

const Orders = ({ orders, getOrders, deleteOrder }) => {
	useEffect(() => {
		getOrders();
	}, []);

	const onDelete = (_id) => {
		fetch(process.env.SERVER + "api/orders/" + _id, {
			method: "DELETE",
		})
			.then((res) => res.json())
			.then((result) => {
				if (result?.deletedCount === 1) {
					deleteOrder(_id);
				}
			});
	};

	return (
		<div className="orders-page">
			<Header />
			{orders.map((o) => {
				return (
					<div className="ordered-item" key={o._id}>
						<div className="ordered-item-actions">
							<Link to={{ pathname: "/products", state: { _id: o._id } }}>
								Edit
							</Link>
							<button className="btn" onClick={() => onDelete(o._id)}>
								delete
							</button>
						</div>
						<div>
							<div className="ordered-item-block">
								{Object.keys(o)
									.map((_k) =>
										!isNaN(o[_k]) && _k !== "__v" ? (
											<span key={_k}>
												{_k}: {o[_k]}
											</span>
										) : null
									)
									.filter((_o) => _o)}
							</div>
						</div>
					</div>
				);
			})}
		</div>
	);
};

const enhance = connect(
	(state) => ({
		orders: getOrdersSelector(state),
	}),
	{
		getOrders: getOrdersAction,
		deleteOrder: deleteOrderAction,
	}
);

export default enhance(Orders);
