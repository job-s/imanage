var fs = require("fs");
var path = require("path");
var webpack = require("webpack");

var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var miniCssExtractPlugin = new MiniCssExtractPlugin({ filename: "index.css" });
var WebpackNotifierPlugin = require("webpack-notifier");
var webpackNotifierPlugin = new WebpackNotifierPlugin({
	title: "Title",
	alwaysNotify: true,
});
var environmentPlugin = new webpack.EnvironmentPlugin(["SERVER"]);

module.exports = {
	mode: process.env.NODE_ENV === "development" ? "development" : "production",
	devtool:
		process.env.NODE_ENV === "development" ? "inline-source-map" : "source-map",
	entry: __dirname + "/src/index.js",
	output: {
		path: __dirname + "/public/assets",
		publicPath: "assets",
		filename: "index.js",
	},
	devServer: {
		inline: true,
		contentBase: "./public",
		port: process.env.PORT,
		historyApiFallback: true,
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader",
			},
			{
				test: /\.json$/,
				exclude: /node_modules/,
				loader: "json-loader",
			},
			{
				test: /\.(sc|c)ss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: process.env.NODE_ENV === "development",
							reloadAll: true,
						},
					},
					"css-loader",
					"postcss-loader",
					{
						loader: "sass-loader",
						options: {
							includePaths: ["src/common/scss"],
							implementation: require("sass"),
						},
					},
				],
			},
		],
	},
	plugins: [miniCssExtractPlugin, webpackNotifierPlugin, environmentPlugin],
	resolve: {
		alias: {
			src: path.resolve(__dirname, "src/"),
		},
	},
};
