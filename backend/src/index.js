import express from "express";
import mongoose from "mongoose";
import cors from "cors";

import ordersRouter from "./orders/ordersController";

mongoose.connect(
	"mongodb://localhost:27017/subenimanage",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	},
	(err) => {
		if (err) {
			console.error("Mongo DB Connection Error!");
			console.error(err);
		} else {
			console.log("Mongo DB Connected");
		}
	}
);

const app = express();
const apiRouter = express.Router();

app.use(express.static("public"));
app.use("/api", apiRouter);

apiRouter.use(cors());

apiRouter.use("/orders", ordersRouter);

app.listen(process.env.PORT, (error) => {
	if (!error) {
		console.log("#############################");
		console.log("server running on port: ", process.env.PORT);
		console.log("#############################");
	}
});
