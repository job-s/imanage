export const productsSchema = {
	type: "object",
	properties: {
		Food: {
			type: "string",
			enum: ["Lentil", "Rice", "Milk", "Vegetables"],
		},
		Lentil: {
			type: "string",
			enum: ["Masoor Dal", "Toor/Arhar Dal", "Lobia", "Chickpea"],
		},
		Rice: {
			type: "string",
			enum: ["Basmati Rice", "Brown Rice", "Jasmine Rice"],
		},
		Milk: {
			type: "string",
			enum: ["Silm Milk", "Cow Milk"],
		},
		Vegetables: {
			type: "string",
			enum: ["Tomato", "Potato", "Peas"],
		},
		Utensils: {
			type: "string",
			enum: ["Tea sets", "Spoons", "Plates"],
		},
		Toys: {
			type: "string",
			enum: ["Electric toys", "Board games", "Card games"],
		},
		"Masoor Dal": {
			type: "integer",
			minimum: 1,
		},
		"Toor/Arhar Dal": {
			type: "integer",
			minimum: 1,
		},
		Lobia: {
			type: "integer",
			minimum: 1,
		},
		Chickpea: {
			type: "integer",
			minimum: 1,
		},
		"Basmati Rice": {
			type: "integer",
			minimum: 1,
		},
		"Brown Rice": {
			type: "integer",
			minimum: 1,
		},
		"Jasmine Rice": {
			type: "integer",
			minimum: 1,
		},
		"Silm Milk": {
			type: "integer",
			minimum: 1,
		},
		"Cow Milk": {
			type: "integer",
			minimum: 1,
		},
		Tomato: {
			type: "integer",
			minimum: 1,
		},
		Potato: {
			type: "integer",
			minimum: 1,
		},
		Peas: {
			type: "integer",
			minimum: 1,
		},
		"Tea sets": {
			type: "integer",
			minimum: 1,
		},
		Spoons: {
			type: "integer",
			minimum: 1,
		},
		Plates: {
			type: "integer",
			minimum: 1,
		},
		"Electric toys": {
			type: "integer",
			minimum: 1,
		},
		"Board games": {
			type: "integer",
			minimum: 1,
		},
		"Card games": {
			type: "integer",
			minimum: 1,
		},
	},
	anyOf: [
		{ required: ["Masoor Dal"] },
		{ required: ["Toor/Arhar Dal"] },
		{ required: ["Lobia"] },
		{ required: ["Chickpea"] },
		{ required: ["Basmati Rice"] },
		{ required: ["Brown Rice"] },
		{ required: ["Jasmine Rice"] },
		{ required: ["Silm Milk"] },
		{ required: ["Cow Milk"] },
		{ required: ["Tomato"] },
		{ required: ["Potato"] },
		{ required: ["Peas"] },
		{ required: ["Tea sets"] },
		{ required: ["Spoons"] },
		{ required: ["Plates"] },
		{ required: ["Electric toys"] },
		{ required: ["Board games"] },
		{ required: ["Card games"] },
	],
};
