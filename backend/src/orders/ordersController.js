import { create, findAll, updateById, deleteById } from "./ordersModel";
import { checkProducts } from "./ordersMiddleware";
import express from "express";
import bodyParser from "body-parser";

const ordersRouter = express.Router();
ordersRouter.use(bodyParser.json());

ordersRouter.put("/placeorder", checkProducts(), async (req, res) => {
	try {
		const entry = await create({ ...req.body });
		res.send(entry);
	} catch (error) {
		console.error(error);
		res.status(500).send();
	}
});

ordersRouter.get("/", async (req, res) => {
	try {
		const entries = await findAll({});
		res.send(entries);
	} catch (error) {
		console.error(error);
		res.status(500).send();
	}
});

ordersRouter.post("/update/:id", checkProducts(), async (req, res) => {
	try {
		const { id } = req.params;
		const entry = await updateById(id, { ...req.body });
		res.send(entry);
	} catch (error) {
		console.error(error);
		res.status(500).send();
	}
});

ordersRouter.delete("/:id", async (req, res) => {
	try {
		const { id } = req.params;
		const result = await deleteById(id);
		res.send(result);
	} catch (error) {
		console.error(error);
		res.status(500).send();
	}
});

export default ordersRouter;
