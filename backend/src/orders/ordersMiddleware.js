import { productsSchema } from "./ordersSchemas";
import Ajv from "ajv";

export const checkProducts = (options) => {
	const ajv = new Ajv();
	const validate = ajv.compile(productsSchema);

	return function (req, res, next) {
		if (validate(req.body)) {
			console.log("middleware pass");
			next();
			return;
		}
		res.status(400).send({ message: "Invalid schema format." });
	};
};
