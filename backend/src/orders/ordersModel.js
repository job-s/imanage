import mongoose from "mongoose";

const OrderSchema = mongoose.Schema(
	{
		_id: { type: mongoose.Schema.Types.ObjectId, required: true, auto: true },
		// Food: { type: String },
		// Lentil: { type: String },
		// Rice: { type: String },
		// Milk: { type: String },
		// Vegetables: { type: String },
		// Utensils: { type: String },
		// Toys: { type: String },
		// "Masoor Dal": { type: Number },
		// "Toor/Arhar Dal": { type: Number },
		// Lobia: { type: Number },
		// Chickpea: { type: Number },
		// "Basmati Rice": { type: Number },
		// "Brown Rice": { type: Number },
		// "Jasmine Rice": { type: Number },
		// "Silm Milk": { type: Number },
		// "Cow Milk": { type: Number },
		// Tomato: { type: Number },
		// Potato: { type: Number },
		// Peas: { type: Number },
		// "Tea sets": { type: Number },
		// Spoons: { type: Number },
		// Plates: { type: Number },
		// "Electric toys": { type: Number },
		// "Board games": { type: Number },
		// "Card games": { type: Number },
	},
	{ strict: false }
);

const Order = mongoose.model("Orders", OrderSchema, "Orders");

export const create = async function (obj) {
	const order = new Order(obj);
	return await order.save();
};

export const findAll = async function () {
	const result = await Order.find({}).lean();
	return result;
};

export const updateById = async function (_id, obj) {
	const result = await Order.updateOne({ _id }, obj);
	return result;
};

export const deleteById = async function (_id) {
	const result = await Order.deleteOne({ _id });
	return result;
};

export default Order;
