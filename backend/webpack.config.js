var fs = require("fs");
var path = require("path");

var webpack = require("webpack");

var WebpackNotifierPlugin = require("webpack-notifier");
var webpackNotifierPlugin = new WebpackNotifierPlugin({
	title: "Title",
	alwaysNotify: true,
});

module.exports = {
	mode: process.env.NODE_ENV === "development" ? "development" : "production",
	devtool:
		process.env.NODE_ENV === "development" ? "inline-source-map" : "source-map",
	entry: __dirname + "/src/index.js",
	target: "node",
	output: {
		path: __dirname,
		filename: "dist/server.js",
		libraryTarget: "commonjs2",
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader",
			},
			{
				test: /\.json$/,
				exclude: /node_modules/,
				loader: "json-loader",
			},
			{
				test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
				loader: "file-loader",
				options: {
					name: "public/media/[name].[ext]",
					publicPath: (url) => url.replace(/public/, ""),
					emit: false,
				},
			},
		],
	},
	plugins: [webpackNotifierPlugin],
	resolve: {
		alias: {
			src: path.resolve(__dirname, "src/"),
		},
	},
};
